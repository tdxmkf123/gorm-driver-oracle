module gitee.com/tdxmkf123/gorm-driver-oracle

go 1.20

require (
	github.com/emirpasic/gods v1.18.1
	github.com/thoas/go-funk v0.8.0
	gorm.io/gorm v1.25.1
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
